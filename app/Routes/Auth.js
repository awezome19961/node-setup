const express = require('express');
const router = express.Router();
const { login, logout, register, forgotPassword } = require("../Controller/Authentication")

router.route("/login").post(login)
router.route("/register").post(register)
router.route("/logout").get(logout)
router.route("/forgot-password").post(forgotPassword)

module.exports = router